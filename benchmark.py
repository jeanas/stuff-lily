#!/usr/bin/env python3

import argparse
import atexit
import itertools
import math
import re
import statistics
import subprocess

from rich.console import Console
from rich.layout import Layout
from rich.live import Live
from rich.table import Table
from rich import box

p = argparse.ArgumentParser()

p.add_argument("file", help=".ly file to use for benchmarking")
p.add_argument(
    "-n",
    type=int,
    default=None,
    help="number of times to benchmark (default: stop on Ctrl-C)",
)
p.add_argument("--lily-flags", default="", help="extra flags to pass to lilypond")
p.add_argument(
    "-e",
    "--lily-executable",
    default="build/out/bin/lilypond",
    help="lilypond executable to benchmark",
)
p.add_argument(
    "-b",
    "--baseline-executable",
    default="master/build/out/bin/lilypond",
    help="executable to compare against",
)

args = p.parse_args()

print(
    f"Benchmarks of {args.lily_executable} against {args.baseline_executable} on {args.file}"
)

with open("/proc/cpuinfo") as f:
    m = re.search(r"model name\t: (.*)", f.read())
print(f"CPU type: {m.group(1)}")

result = subprocess.run(
    ["cpupower", "frequency-info", "-l"], stdout=subprocess.PIPE, text=True, check=True
).stdout
cpu_min_str, cpu_max_str = result.splitlines()[1].split()
cpu_min = int(cpu_min_str)
cpu_max = int(cpu_max_str)

new_freq = max(cpu_max // 2, cpu_min)
print(f"CPU frequency set to {new_freq//1000} MHz")
cmd_args = [
    "sudo",
    "-n",
    "cpupower",
    "frequency-set",
    "-u",
    str(new_freq),
    "-d",
    str(new_freq),
]
subprocess.run(cmd_args, stdout=subprocess.DEVNULL, check=True)


@atexit.register
def reset_cpu():
    cmd_args = [
        "sudo",
        "-n",
        "cpupower",
        "frequency-set",
        "-u",
        str(cpu_max_str),
        "-d",
        str(cpu_min_str),
    ]
    subprocess.run(cmd_args, check=True)


def benchmark(executable):
    cmd = [
        "/usr/bin/time",
        "-f",
        r"%e\n%M",
        executable,
        "-s",
        *args.lily_flags.split(),
        args.file,
    ]
    result = subprocess.run(cmd, stderr=subprocess.PIPE, text=True).stderr
    try:
        time_str, mem_str = result.splitlines()
    except ValueError:
        print(f"Command returned bad output:")
        print(result)
        raise SystemExit
    return float(time_str), int(mem_str)


def results(lst):
    the_min = min(lst)
    the_mean = statistics.mean(lst)
    if len(lst) > 1:
        the_stderr = statistics.stdev(lst) / math.sqrt(len(lst))
    else:
        the_stderr = math.inf
    return the_min, the_mean, the_stderr


def human_listify_floats(lst):
    return f", ".join(f"{x:.2f}s" for x in lst)


def human_listify_ints(lst):
    return f", ".join(str(x) for x in lst)


def report_results(lsta, lstb, rtype):
    summary = Table(title=f"{rtype.capitalize()} summary", box=box.ASCII)
    summary.add_column("")
    summary.add_column("Baseline")
    summary.add_column("New executable")
    summary.add_column("Delta")
    delta_info = ""
    if lsta:
        resultsa = results(lsta)
        mina, meana, stderra = resultsa
        f_resultsa = [f"{x:.3f}" for x in resultsa]
        resultsb = results(lstb)
        minb, meanb, stderrb = resultsb
        f_resultsb = [f"{x:.3f}" for x in resultsb]
        delta_min = f"{(minb-mina)/mina*100:.2f}%"
        delta_mean = f"{(meanb-meana)/meana*100:.2f}%"
        if len(lsta) > 1 and (stderra > 0 or stderrb > 0):
            delta_info = f"Difference of means is {abs(meanb-meana)/((stderra+stderrb)/2):.2f} times standard error.\n\n"
        f_delta = [delta_min, delta_mean, ""]
    else:
        f_resultsa = f_resultsb = f_delta = ["N/A"] * 3
    for indicator, ra, rb, delta in zip(
        ("Min", "Mean", "Standard error"), f_resultsa, f_resultsb, f_delta
    ):
        summary.add_row(indicator, ra, rb, delta)
    if rtype == "time":
        raw_list_a = human_listify_floats(lsta)
        raw_list_b = human_listify_floats(lstb)
    else:
        assert rtype == "memory"
        raw_list_a = human_listify_ints(lsta)
        raw_list_b = human_listify_ints(lstb)
    raw = Layout(
        delta_info
        + f"Raw {rtype} measurements:\nBaseline: {raw_list_a}\nNew executable: {raw_list_b}"
    )
    layout = Layout()
    layout.split_column(summary, raw)
    return layout


def make_layout(i, lsta, lstb):
    counter = Layout(f"{i} runs", size=2)
    layout = Layout()
    sublayout = Layout()
    time_layout = report_results([x[0] for x in lsta], [x[0] for x in lstb], "time")
    mem_layout = report_results([x[1] for x in lsta], [x[1] for x in lstb], "memory")
    layout.split_column(counter, sublayout)
    sublayout.split_column(time_layout, mem_layout)
    return layout


i = 0
lsta = []
lstb = []

console = Console(highlight=False, markup=False)

try:
    with Live(
        make_layout(0, lsta, lstb), vertical_overflow="visible", console=console
    ) as live:
        if args.n is None:
            iterator = itertools.count(1)
        else:
            iterator = range(1, args.n + 1)
        for count in iterator:
            t = benchmark(args.lily_executable)
            b = benchmark(args.baseline_executable)
            lsta.append(t)
            lstb.append(b)
            live.update(make_layout(count, lsta, lstb))
except KeyboardInterrupt:
    pass
