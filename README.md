This repository contains personal unsorted LilyPond development stuff,
much of it work-in-progress.  For convenience and given that nothing
here is precious, the license is the "unlicense": you are allowed to
do whatever you wish with what you find here.

Currently, this repo contains:

- `benchmark.py`, a Python script to benchmark a LilyPond executable
  against another. This is tailored for Linux and would need adapting
  elsewhere.

  To use it, type

  ```
  python3 benchmark.py -e /executable/to/test/lilypond -b /baseline/executable/lilypond benchmark-score.ly
  ```

  This requires the `rich` module from PyPI. To obtain it, use

  ```
  python3 -m pip install --user --upgrade rich
  ```

  (If you get an error that Pip is not installed, try installing it
  via the package manager, e.g., `sudo apt install python3-pip`.)

  You also need the `cpupower` tool to pin CPU frequency
  temporarily. On Ubuntu, this is provided by

  ```
  sudo apt install cpufrequtils
  ```

  `benchmark-score.ly` can be any LilyPond file you like, but for
  meaningful benchmarking you would normally choose a file that is not
  too small (unless you are interested in startup time).
  Possibilities include `input/regression/mozart-hrn-3.ly` from the
  official repository, `MSDM.ly` from
  https://lists.gnu.org/archive/html/lilypond-user/2016-11/msg00700.html,
  `B9-4.ly` from http://lanfear.me/Beethoven/, and Les Fêtes
  vénitiennes from
  https://lists.gnu.org/archive/html/lilypond-user/2016-11/msg00948.html.
  (Needless to say, run `convert-ly` before compiling any of those
  those except the one in the main repository.)

- `lilymwe.py`, a tiny script that reads LilyPond code from standard
  input, compiles it into a temporary file using `-dcrop`, and opens
  the result in Eye of GNOME. Handy for issue reports.

- `db.ly`, an includable file defining a small tool I frequently use
  for debugging Scheme code. It defines a `db` macro. When `db` is
  used as `(db (item 1 ...) (item 2 ...) ...)`, it prints the values
  of its arguments along with the code that generated them, and
  returns the value of the last argument. There is also a reader
  extension `#?`. The code `#?expression` is equivalent to `(db
  expression)`.

### Why 'stuff-lily' and not 'lily-stuff'?

Because 'lily-stuff' would force me to type as much as 'lilyp' to get
autocompletion to fill 'lilypond' every time I `cd` into the main
repository :-)
