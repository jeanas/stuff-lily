#!/usr/bin/env python3

import argparse
import subprocess
import sys

p = argparse.ArgumentParser()
p.add_argument("-e", "--executable", default="build/out/bin/lilypond")
p.add_argument("--editor", default="eog")
args = p.parse_args()

print("Enter code:")
code = sys.stdin.read()

try:
    subprocess.run([args.executable, "--png", "-dcrop", "-o", "/tmp/mwe", "-"], input=code, text=True, check=True)
except subprocess.CalledProcessError:
    pass
else:
    subprocess.Popen([args.editor, "/tmp/mwe.cropped.png"], start_new_session=True)
