\version "2.23.8"

%% Use this include file to add a db macro to facilitate debugging.
%% Can also be used as reader extension.

#(define-syntax-rule (db arg ...)
   (begin
     (let ((evald-arg arg))
       (ly:message ";;; ~s => ~s" (quote arg) evald-arg)
       evald-arg)
     ...))

#(read-hash-extend
  #\?
  (lambda (char port)
    #`(db #,(read-syntax port))))
